# Assignment 2-3, INF226

## 2A
### Analyze of the structure
* The first thing you notice is that the code is a big mess. All the files are put in the same main folder, so it is hard to know what belongs where. There can definetly be made improvements by splitting the app.py-file into several files. It is also convinient to seperate the stylesheets into their own files, this makes the html-files smaller, and therefore more readable.
* By looking more into flask, I found that they have a "standard" project setup. The provided code does not follow this "standard", and by adapting to this "standard" the project will be easier to navigate through, both by yourself and others. This will also cause the project to be harder to extend.
* The points mentioned above is not a big security flaw in it self, but can cause some trouble. The messy structure makes it hard to analyze the code for security flaws, so security flaws have a bigger chance of staying unnoticed. 
* Refactoring:
   * Removed a useless file
   * Moved index.html into the mentioned templates-directory
   * Moved the favicon-files into the mentioned static-directory
   * Did some minor cleaning here and there

### Analyze of security flaws
* The messaging app is vulnerable to SQL-injections
* The messaging app is vulnerable to XSS

## 2B
### Documentation
* New features
   * Login now checks password, this was not earlier implemented
   * Signupform, where anyone can create their own user. New user cant have the same username as someone else, and passwords need to be longer than 8 characters, have at least 1 capital letter and at least one number.
   * Logoutbutton. Once you logged in earlier, there was no way to log out to the login screen, this is now implemented.
   * You can now only see and search for messages to or from yourself, earlier you could watch anyones messages.
   * Sat up a database, where users can be stored. Passwords are hashed.

### Questions
* Who might attack the application?
   * There can be several attackers that might be interested in attacking this messageapplication. Some attackers wants to find information about the users. This can be private messages with sensitive information that they don't want to share to others than the reciever. These attackers can also be interested in userinformation, such as email, password etc., so that they can misuse these to enter other applications the user might be on. Some other attackers might just want to deny the service of the application. This will be a problem for the owner, since the application might loose users if this is a repeating problem.
   This application is using both scripts and SQL, therefore it is important to protect the application from cross-site-scripting and SQL-injections since there are several inputfields that would make this possible.
   In terms of confidentiality, damage can be done by finding sensitive information about others.
   In terms of intergrity, damage can be done by sending messages as somebody else.
   In terms of availability, damage can be done by either shutting down the whole application, or removing messages from a given conversation.
* What are the main attack vectors for the application?
   * I would say the main attackvectors are XSS and SQL-injections as i mentioned earlier.
   * DDoS is also relevant when talking attack vectors for thsi application. This is because the server is not the greatest, so overloading the server with many machines would probably cause the server and application to crash.
   * As mentioned the server is not the greatest, so a hacker could maybe find some unpatched holes in the server.
* What should we do (or what have you done) to protect against attacks?
   * Regarding XSS there are a couple of things to do for protection. The first thing is filtering input as much as possible, based on what is expected or valid. Secondly we should encode the data on output.
   * Regarding SQL-injections, most attacks can be prevented by parameterized statements, also known as prepared statements. The advantage of this is that you avoid string concatenation by the user, which is the biggest mistake with SQL-injections.
* What is the access control model?
   * The access control model is Discretionary Acces Control
   * Only sender and recipent will be able to view the messages between them